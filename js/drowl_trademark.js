(function (Drupal, $, once) {
  /**
   * Initialization
   */
  Drupal.behaviors.drowl_trademark = {
    /**
     * Run Drupal module JS initialization.
     *
     * @param context The page context.
     * @param settings The Drupal settings.
     */
    attach(context, settings) {
      // #webksde#JP20141015: Do not handle email addresses and elements with container class ".no-replacetext".
      // #webksde#JP20150813: once ergänzt - ACHTUNG - setzt sehr viele classes, sodass es hier zu Performance-Problemen kommen kann. Bei Problemen wieder entfernen!
      // WICHTIG: Die <sup - Erkennung "(?!\<sup)" des Folgewortes funktioniert hier nicht, da der Regex nur auf einzelne Texte, nicht aber auf folgende HTML Elemente angewandt wird!
      const { replacepattern } = settings.drowl_trademark;
      const { filter } = settings.drowl_trademark;
      const regexp = new RegExp(`\\b(${replacepattern})(?!\\<sup)\\b`, "gi");
      if (context !== document) {
        // Replace in AJAX content
        $(once("drowl-trademark", "*", context))
          .not(filter)
          .replaceText(regexp, "$1<sup>®</sup>", false);
      } else {
        // Replace in body (e.g. on page load)
        $(once("drowl-trademark", "body *"))
          .not(filter)
          .replaceText(regexp, "$1<sup>®</sup>", false);
      }
    },
  };

  /*
   * jQuery replaceText - v1.1 - 11/21/2009
   * http://benalman.com/projects/jquery-replacetext-plugin/
   *
   * Copyright (c) 2009 "Cowboy" Ben Alman
   * Dual licensed under the MIT and GPL licenses.
   * http://benalman.com/about/license/
   */
  $.fn.replaceText = function (b, a, c) {
    return this.each(function () {
      let f = this.firstChild;
      let g;
      let e;
      const d = [];
      if (f) {
        do {
          if (f.nodeType === 3) {
            g = f.nodeValue;
            e = g.replace(b, a);
            if (e !== g) {
              if (!c && /</.test(e)) {
                $(f).before(e);
                d.push(f);
              } else {
                f.nodeValue = e;
              }
            }
          }
        } while ((f = f.nextSibling));
      }
      d.length && $(d).remove();
    });
  };
})(Drupal, jQuery, once);
