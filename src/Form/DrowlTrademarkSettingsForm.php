<?php

namespace Drupal\drowl_trademark\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Modules Settings Form.
 */
class DrowlTrademarkSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drowl_trademark_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drowl_trademark.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['drowl_trademark_replacements'] = [
      '#type' => 'textfield',
      '#title' => 'Append ® to these words',
      '#description' => $this->t("Enter words comma separated to append a <sup>®</sup> to these words dynamically at runtime."),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_trademark.settings')->get('drowl_trademark_replacements'),
    ];

    $form['drowl_trademark_filter'] = [
      '#type' => 'textfield',
      '#title' => 'Filter by jQuery filters (exclusion)',
      '#description' => $this->t('Use jQuery filters to skip matching elements, separated by comma. Regular jQuery notation. Default: ".no-drowl-trademark,a[itemprop=email]".<br>To include parents, e.g. for the spamspan module, you may use the unperformant ".spamspan > *" filter.'),
      '#required' => TRUE,
      '#default_value' => $this->config('drowl_trademark.settings')->get('drowl_trademark_filter'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('drowl_trademark.settings');
    $config->set('drowl_trademark_replacements', $form_state->getValue('drowl_trademark_replacements'));
    $config->set('drowl_trademark_filter', $form_state->getValue('drowl_trademark_filter'));
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

}
