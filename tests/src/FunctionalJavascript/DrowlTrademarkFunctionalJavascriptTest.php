<?php

namespace Drupal\Tests\drowl_trademark\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group drowl_trademark
 */
class DrowlTrademarkFunctionalJavascriptTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'test_page_test',
    'editor',
    'filter',
    'drowl_trademark',
    'drowl_trademark_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->config('drowl_trademark.settings')->set('drowl_trademark_replacements', 'test')->save();
    $this->createContentType(['type' => 'article']);

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests, if the trademark symbol gets appended to a word correctly.
   */
  public function testTrademark() {
    $session = $this->assertSession();

    $this->createNode([
      'type' => 'article',
      'id' => 1,
      'title' => 'test',
      'body' => [
        [
          'value' => '<p>prefix test suffix</p>',
          'format' => 'full_html',
        ],
      ],
    ]);

    $this->drupalGet('/node/1');

    // Ensure the Javascript already ran:
    $session->waitForElementVisible('css', 'h1 > span sup');

    // Test the title. Expected to contain ® suffix:
    $session->elementTextEquals('css', 'h1 > span', 'test®');
    $session->elementContains('css', 'h1 > span', 'test<sup>®</sup>');

    // Test the body. Expected to contain ® suffix:
    $session->elementTextEquals('css', 'article > div > div > p', 'prefix test® suffix');
    $session->elementContains('css', 'article > div > div > p', 'prefix test<sup>®</sup> suffix');
  }

  /**
   * Tests, if the trademark symbol does not get appended if it's inside a word.
   */
  public function testTrademarkPartOfWord() {
    $session = $this->assertSession();

    $this->createNode([
      'type' => 'article',
      'id' => 1,
      'title' => 'prefix test suffix',
      'body' => [
        [
          'value' => 'prefix testabc suffix',
          'format' => filter_default_format(),
        ],
      ],
    ]);

    $this->drupalGet('/node/1');

    // Ensure the Javascript already ran:
    $session->waitForElementVisible('css', 'h1 > span sup');

    // Test the title. Expected to contain ® suffix:
    $session->elementTextEquals('css', 'h1 > span', 'prefix test® suffix');
    $session->elementContains('css', 'h1 > span', 'prefix test<sup>®</sup> suffix');

    // Test the body. Expected to NOT contain ® suffix:
    $session->elementTextEquals('css', 'article > div > div', 'prefix testabc suffix');
  }

  /**
   * Tests, if the trademark symbol does not get appended in an excluded class.
   */
  public function testTrademarkInExcludedClass() {
    $session = $this->assertSession();

    $this->createNode([
      'type' => 'article',
      'id' => 1,
      'title' => 'test',
      'body' => [
        [
          'value' => '<p><span class="no-drowl-trademark">prefix test suffix</span></p>',
          'format' => 'full_html',
        ],
      ],
    ]);

    $this->drupalGet('/node/1');

    // Ensure the Javascript already ran:
    $session->waitForElementVisible('css', 'h1 > span sup');

    // Test the title. Expected to contain ® suffix:
    $session->elementTextEquals('css', 'h1 > span', 'test®');
    $session->elementContains('css', 'h1 > span', 'test<sup>®</sup>');

    // Test the body. Expected to NOT contain ® suffix:
    $session->elementTextEquals('css', 'article > div > div > p', 'prefix test suffix');
    $session->elementContains('css', 'article > div > div > p', '<span class="no-drowl-trademark">prefix test suffix</span>');
  }

}
