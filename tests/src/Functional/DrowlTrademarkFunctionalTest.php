<?php

namespace Drupal\Tests\drowl_trademark\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods for testing drowl trademark functionalities.
 *
 * @group drowl_trademark
 */
class DrowlTrademarkFunctionalTest extends BrowserTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'drowl_trademark',
    'test_page_test',
    'node',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-drowl-trademark');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests "administer drowl trademark" permission.
   */
  public function testAccess() {
    $session = $this->assertSession();
    // Check access as admin:
    $this->drupalGet('/admin/config/user-interface/drowl_trademark');
    $session->statusCodeEquals(200);
    $this->drupalLogout();
    // Check access as user with correct permissions:
    $this->drupalLogin($this->drupalCreateUser(['administer drowl trademark']));
    $this->drupalGet('/admin/config/user-interface/drowl_trademark');
    $session->statusCodeEquals(200);
    $this->drupalLogout();
    // Check access as user without permissions:
    $this->drupalLogin($this->user);
    $this->drupalGet('/admin/config/user-interface/drowl_trademark');
    $session->statusCodeEquals(403);
    $this->drupalLogout();
    // Check access as anonymous:
    $this->drupalGet('/admin/config/user-interface/drowl_trademark');
    $session->statusCodeEquals(403);
  }

  /**
   * Tests disabling a layout.
   */
  public function testTrademark() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Got to config site and set configs:
    $this->drupalGet('/admin/config/user-interface/drowl_trademark');
    $session->statusCodeEquals(200);
    $session->elementAttributeContains('css', '#edit-drowl-trademark-filter', 'value', ".no-drowl-trademark,.no-drowl-trademark *,a[itemprop=email] *,a[href^='mailto:'] *,.spamspan *");
    $page->fillField('edit-drowl-trademark-replacements', 'Test page');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The configuration options have been saved.');
    $this->drupalGet('<front>');
    $session->elementExists('css', 'script[src*=drowl_trademark]');
  }

}
